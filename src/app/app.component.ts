import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ExperienceItem} from './experience/item/experience-item';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Profile';
  experience: ExperienceItem[] = [];

  constructor(private httpClient: HttpClient) {
  }

  ngOnInit() {
    this.httpClient.get('assets/data/experience.json').subscribe(
      (data: ExperienceItem[]) => {
        this.experience = data;
      }
    );
  }
}
