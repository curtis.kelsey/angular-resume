import {Component, OnInit} from '@angular/core';
import {BookListItem} from './book/book-list-item';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit {
  bookListItems: BookListItem[] = [
    {
      title: 'Refactoring: Improving the Design of Existing Code',
      author: 'Martin Fowler'
    },
    {
      title: 'The Pragmatic Programmer',
      author: 'David Thomas & Andrew Hunt'
    },
    {
      title: 'Radical Candor',
      author: 'Kim Scott'
    },
    {
      title: 'Advanced Web Application Architecture',
      author: 'Matthias Noback'
    },
    {
      title: 'Peopleware',
      author: 'Tom Demarco & Timothy Lister'
    }
  ];

  pageOne: BookListItem[];
  pageTwo: BookListItem[];

  constructor() { }

  ngOnInit(): void {
    this.updateBookList();
  }

  updateBookList(): void {
    this.pageOne = this.bookListItems.slice(0, 3);
    this.pageTwo = this.bookListItems.slice(3, 5);
  }
}
