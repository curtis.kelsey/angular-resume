export interface BookListItem {
  title: string;
  author: string;
}
