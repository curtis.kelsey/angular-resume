import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-terminal',
  templateUrl: './terminal.component.html',
  styleUrls: ['./terminal.component.scss']
})
export class TerminalComponent implements OnInit {
  @Input() text = 'This is a terminal';
  displayText = '';
  running = false;

  constructor() {
  }

  ngOnInit(): void {
    this.showText(0, 30);
  }

  showText(index: number, interval: number): void {
    if (this.running && index === 0) {
      return;
    }

    this.running = true;

    if (index === 0) {
      this.displayText = '';
    }

    if (index >= this.text.length) {
      this.running = false;
      return;
    }

    const punctuation = '\n';
    const newCharacter = this.text[index++];
    let currentInterval = interval;

    if (punctuation.includes(newCharacter)) {
      currentInterval += 300;
    }

    this.displayText += newCharacter;
    setTimeout(
      () => {
        this.showText(index, interval);
      },
      currentInterval
    );
  }
}
