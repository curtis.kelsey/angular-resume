import {AfterContentInit, Component} from '@angular/core';
import * as d3 from 'd3';
import 'd3-cloud/index';
import {WordItem} from './word-item';

@Component({
  selector: 'app-d3-word-cloud',
  templateUrl: './d3-word-cloud.component.html',
  styleUrls: ['./d3-word-cloud.component.scss']
})
export class D3WordCloudComponent implements AfterContentInit {

  constructor() {
  }

  ngAfterContentInit(): void {
    // const frequencyList: WordItem[] = [
    //   {text: 'study', size: 40},
    //   {text: 'motion', size: 15},
    //   {text: 'forces', size: 10},
    //   {text: 'electricity', size: 15},
    //   {text: 'movement', size: 10},
    //   {text: 'relation', size: 5},
    //   {text: 'things', size: 10},
    //   {text: 'force', size: 5},
    //   {text: 'ad', size: 5}
    // ];

    // const color = d3.scale.linear()
    //   .domain([0, 1, 2, 3, 4, 5, 6, 10, 15, 20, 100])
    //   .range([4444, 3333, 2222, 1111, 999, 888, 777, 666, 555, 444, 333, 222]);
    //
    // function draw(words) {
    //   d3.select('.word-cloud-container').append('svg')
    //     .attr('width', 850)
    //     .attr('height', 350)
    //     .attr('class', 'wordcloud')
    //     .append('g')
    //     // without the transform, words words would get cutoff to the left and top, they would
    //     // appear outside of the SVG area
    //     .attr('transform', 'translate(320,200)')
    //     .selectAll('text')
    //     .data(words)
    //     .enter().append('text')
    //     .style('font-size', (d: WordItem) => d && d.size ? d.size : 0 + 'px')
    //     .style('fill', (d, i) => color(i))
    //     .attr('transform', (d) => {
    //       return 'translate(' + [d.x, d.y] + ')rotate(' + d.rotate + ')';
    //     })
    //     .text((d: WordItem) => d.text);
    // }
    //
    // d3.layout.cloud().size([800, 300])
    //   .words(frequencyList)
    //   .rotate(0)
    //   .fontSize((d) => d.size)
    //   .on('end', draw)
    //   .start();
  }
}
