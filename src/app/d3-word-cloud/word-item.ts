export interface WordItem {
  text: string;
  size: number;
}
