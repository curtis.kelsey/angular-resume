import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-education-item',
  templateUrl: './education-item.component.html',
  styleUrls: ['./education-item.component.scss']
})
export class EducationItemComponent implements OnInit {
  @Input() degree: string;
  @Input() date: string;
  @Input() university: string;
  @Input() location: string;

  constructor() { }

  ngOnInit(): void {
  }

}
