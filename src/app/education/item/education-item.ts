export interface EducationItem {
  degree: string;
  date: string;
  university: string;
  location: string;
}
