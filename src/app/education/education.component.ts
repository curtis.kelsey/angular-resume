import { Component, OnInit } from '@angular/core';
import {EducationItem} from './item/education-item';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.scss']
})
export class EducationComponent implements OnInit {
  educationItems: EducationItem[] = [
    {
      degree: 'Master of Science in Computer Science',
      date: 'December 2012',
      university: 'University of Missouri',
      location: 'Columbia, MO'
    },
    {
      degree: 'Bachelor of Science in Computer Science',
      date: 'May 2009',
      university: 'Park University',
      location: 'Parkville, MO'
    },
    {
      degree: 'Associate in Applied Science in Communication Applications Technology',
      date: 'February 2007',
      university: 'Community College of the Air Force',
      location: 'Montgomery, AL'
    }
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
