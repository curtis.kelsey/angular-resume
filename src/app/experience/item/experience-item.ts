import {Bullet} from './bullet';

export interface ExperienceItem {
  title: string;
  dates: string;
  company: string;
  location: string;
  bullets?: Bullet[];
  priority?: number;
}
