import {Component, Inject} from '@angular/core';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {ExperienceItem} from '../experience-item';
import {Bullet} from '../bullet';

@Component({
  selector: 'app-experience-bottom-sheet',
  templateUrl: 'experience-bottom-sheet.html',
  styleUrls: ['./experience-bottom-sheet.scss']
})
export class ExperienceBottomSheetComponent {
  constructor(private bottomSheetRef: MatBottomSheetRef<ExperienceBottomSheetComponent>,
              @Inject(MAT_BOTTOM_SHEET_DATA) public data: ExperienceItem) {
  }

  openLink(event: MouseEvent): void {
    this.bottomSheetRef.dismiss();
  }

  getUrl(bullet: Bullet): string {
    if (bullet.url) {
      return bullet.url;
    }

    return '#';
  }

  getTarget(bullet: Bullet): string {
    if (bullet.url) {
      return '_blank';
    }

    return '_self';
  }
}
