import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ExperienceItemComponent} from './experience-item.component';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';

describe('ExperienceItemComponent', () => {
  let component: ExperienceItemComponent;
  let fixture: ComponentFixture<ExperienceItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ExperienceItemComponent],
      imports: [MatBottomSheetModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExperienceItemComponent);
    component = fixture.componentInstance;
    component.experienceItem = {company: '', dates: '', location: '', title: ''};
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
