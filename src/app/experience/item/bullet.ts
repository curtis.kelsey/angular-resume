export interface Bullet {
  text: string;
  url?: string;
  icon?: string;
}
