import {Component, Input, OnInit} from '@angular/core';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {ExperienceItem} from './experience-item';
import {ExperienceBottomSheetComponent} from './bottom-sheet/experience-bottom-sheet';

@Component({
  selector: 'app-experience-item',
  templateUrl: './experience-item.component.html',
  styleUrls: ['./experience-item.component.scss']
})
export class ExperienceItemComponent implements OnInit {
  @Input() experienceItem: ExperienceItem;
  constructor(private bottomSheet: MatBottomSheet) {
  }

  openBottomSheet(): void {
    if (!this.experienceItem.bullets) {
      return;
    }

    this.bottomSheet.open(ExperienceBottomSheetComponent, {data: this.experienceItem});
  }

  ngOnInit(): void {
  }
}
