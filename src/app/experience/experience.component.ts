import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ExperienceItem} from './item/experience-item';

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss']
})
export class ExperienceComponent implements OnInit, OnChanges {
  @Input() experience: ExperienceItem[];
  pageOne: ExperienceItem[] = [];
  pageTwo: ExperienceItem[] = [];
  constructor() { }

  ngOnInit(): void {
    this.updateExperience();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.experience) {
      this.updateExperience();
    }
  }

  updateExperience(): void {
    if (!this.experience) {
      return;
    }

    this.pageOne = this.experience.slice(0, 5);
    this.pageTwo = this.experience.slice(5, 10);
  }
}
