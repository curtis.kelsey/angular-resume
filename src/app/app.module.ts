import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TerminalComponent } from './terminal/terminal.component';
import {SocialLinkComponent} from './social-carousel/social-link/social-link.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatGridListModule} from '@angular/material/grid-list';
import { SocialCarouselComponent } from './social-carousel/social-carousel.component';
import { EducationItemComponent } from './education/item/education-item.component';
import { EducationComponent } from './education/education.component';
import { CertificationItemComponent } from './certifications/item/certification-item.component';
import { CertificationsComponent } from './certifications/certifications.component';
import { ExperienceComponent } from './experience/experience.component';
import {ExperienceItemComponent} from './experience/item/experience-item.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatListModule} from '@angular/material/list';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {ExperienceBottomSheetComponent} from './experience/item/bottom-sheet/experience-bottom-sheet';
import {D3WordCloudComponent} from './d3-word-cloud/d3-word-cloud.component';
import {HttpClientModule} from '@angular/common/http';
import { BookListComponent } from './book-list/book-list.component';
import { BookListItemComponent} from './book-list/book/book-list-item.component';

@NgModule({
  declarations: [
    AppComponent,
    CertificationItemComponent,
    CertificationsComponent,
    D3WordCloudComponent,
    EducationComponent,
    EducationItemComponent,
    ExperienceComponent,
    ExperienceItemComponent,
    ExperienceBottomSheetComponent,
    SocialLinkComponent,
    SocialCarouselComponent,
    TerminalComponent,
    BookListComponent,
    BookListItemComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    HttpClientModule,
    MatBottomSheetModule,
    MatCardModule,
    MatGridListModule,
    MatIconModule,
    MatListModule,
    MatPaginatorModule
  ],
  providers: [],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
