import { Component, OnInit } from '@angular/core';
import {Certification} from './item/certification';

@Component({
  selector: 'app-certifications',
  templateUrl: './certifications.component.html',
  styleUrls: ['./certifications.component.scss']
})
export class CertificationsComponent implements OnInit {
  certifications: Certification[] = [
    {
      name: 'Microsoft Certified Technology Specialist - Windows 7, Configuration',
      date: 'March 2012',
    },
    {
      name: 'CompTIA Security+',
      date: 'December 2010',
    }
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
