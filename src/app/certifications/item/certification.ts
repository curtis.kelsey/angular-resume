export interface Certification {
  name: string;
  date: string;
}
