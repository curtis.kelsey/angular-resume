import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-certification-item',
  templateUrl: './certification-item.component.html',
  styleUrls: ['./certification-item.component.scss']
})
export class CertificationItemComponent implements OnInit {
  @Input() name: string;
  @Input() date: string;

  constructor() { }

  ngOnInit(): void {
  }

}
