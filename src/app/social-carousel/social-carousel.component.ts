import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-social-carousel',
  templateUrl: './social-carousel.component.html',
  styleUrls: ['./social-carousel.component.scss']
})
export class SocialCarouselComponent implements OnInit {
  noAnimation = 'no-animation';
  spinLock = false;

  constructor() {
  }

  ngOnInit(): void {
  }

  onMouseleave() {
    if (this.spinLock) {
      return;
    }

    this.noAnimation = '';
    this.spinLock = true;

    setTimeout(
      () => {
        this.noAnimation = 'no-animation';
        this.spinLock = false;
      },
      1000
    );
  }
}
